import Vue from 'vue'
import { LoaderPlugin } from 'vue-google-login'

Vue.use(LoaderPlugin, {
  client_id: process.env.googleClientID,
  scope: 'openid profile email https://www.googleapis.com/auth/books'
})

export default function (context) {
  context.$axios.onRequest((config) => {
    const user = context.store.getters.user
    if (user && user.token) {
      config.headers.common.Authorization = `Bearer ${user.token}`
    }
  })
}
