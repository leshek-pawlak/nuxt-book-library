export default function (context) {
  const cookieToken = context.app.$cookies.get('token')
  const user = context.store.getters.user
  if (user && user.token) {
    return
  } else if (cookieToken) {
    context.store.dispatch('setUser', {
      token: cookieToken,
      id: context.app.$cookies.get('userId'),
      avatar: context.app.$cookies.get('userImg'),
      name: context.app.$cookies.get('userName')
    })
    return
  }
  context.redirect('/')
}
