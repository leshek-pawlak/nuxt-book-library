import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      user: null
    },
    mutations: {
      setUser (state, user) {
        state.user = user
      }
    },
    actions: {
      setUser (VuexContext, user) {
        VuexContext.commit('setUser', user)
      }
    },
    getters: {
      user (state) {
        return state.user
      }
    }
  })
}

export default createStore
